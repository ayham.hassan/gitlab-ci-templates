# gitlab-ci-templates

[🇫🇷 French version](README-fr.md)

A collection of useful templates and includes for gitlab-ci.

All files contains one stage and are named `stage-name.yml`

See official documentation: https://docs.gitlab.com/ce/ci/yaml/#include

---
## Repository

### stages.yml

This file contains *stages* definition and *templates* used in jobs.

### job

This directory contains job *templates*.
Each job is folded by type or technology in sub-directory.

---
Icon made by [Freepik](https://www.freepik.com) from [www.flaticon.com](https://www.flaticon.com) is licensed by [CC 3.0 BY](http://creativecommons.org/licenses/by/3.0)
